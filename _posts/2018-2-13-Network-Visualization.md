---
title:  "d3 Network Visualization"
date:   2018-2-13
categories: javascript
layout: default
---

I built a JS visualization tool for my CS networking class, in which we had to
design a program for emulating a network bridge implementing spanning tree
protocol. Having to infer from configuration files and output from the
simulation where a fault lies sucks, so instead I built this tool to help myself
out.

[http://tristansweeney.com/graph-viz](http://tristansweeney.com/graph-viz)

<iframe src="http://tristansweeney.com/graph-viz" width="100%" class="myIframe">
</iframe>
<script type="text/javascript" language="javascript">
  $('.myIframe').css('height', $(window).height()+'px');
</script>
