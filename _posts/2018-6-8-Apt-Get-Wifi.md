---
title:  "Apt install on a Disconnected Wireless System"
date:   2018-6-8
categories: Networking Aptitude
layout: default
---

I just was installing ubuntu on a platform that only has wireless capabilities, and decided to install the server edition to minimize overhead / avoid having an X server + desktop environment to disable. After the installation, I realized one fatal oversight: this platform only has a wifi antenna and no wired ethernet to install packages through. The server edition of Ubuntu ships with no wireless utilities, because nobody in their right mind would run a wireless server.

This was quite the conundrum. I have an Ethernet-\>USB adapter back home, but that's a long bike ride. Instead, I found the tool with the most minimal set of dependencies that suited my needs (wpa\_supplicant, which I've used before on my laptop), copied it's package and it's dependencies packages to a USB drive, mounted that drive, and installed it to the drone. After that, I was able to use wpa\_supplicant and dhclient to connect to my phone (as a hotspot) and install network-manager to use nmcli to connect to my universities enterprise wifi (which I've never figured out in wpa\_supplicant).

So if you're ever stuck needing to connect to the internet like me, just remember that package installations are only a USB away.
