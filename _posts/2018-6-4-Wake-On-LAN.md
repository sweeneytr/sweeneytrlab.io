---
title:  "Wake On LAN"
date:   2018-6-4
categories: Networking
layout: default
---

I've wanted to setup Wake-On-LAN (WOL) in my apartment for a while now, to turn on my desktop (which serves as an FTP/Plex/FOSWiki server) when it's either hibernated/suspended due to inactivity or been left off for one reason or another.

I won't rehash better sources, but the TL;DR is that I enabled WOL locally on my desktop as outlined in the [Arch Wiki](https://wiki.archlinux.org/index.php/Wake-on-LAN), then forwarded a port on my router to an unused IP address and setup an ARP rule which sends all traffic to that address to the broadcast MAC address FF:FF:FF:FF:FF:FF:FF:FF, as outlined in the [DD-WRT Wiki](https://wiki.dd-wrt.com/wiki/index.php/WOL).

WOL packets are broadcast with the intended device to wakeup in the body of the packet, so the only real 'hack' here is having a dedicated IP address acting as an intermediary between the port forwarding and broadcasting. It feels like an unnecissary step, but why fight a wiki's wisdom?

So that's it. This is less a technical post and more of a tiny-brag about my WOL setup (oh look, I can follow directions!) and an record that I do stuff for fun. Oh, and [WolOn](https://play.google.com/store/apps/details?id=com.bitklog.wolon) is a great Android app for hiting the WOL endpoint you setup. Check it out.

Ta ta! 
