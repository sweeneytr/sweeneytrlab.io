---
layout: default
---
# About Me
I'm a full-time firmware engineer, currently working at Amazon Robotics. I
earned my Masters in Computer Engineering in May 2019, with a focus on systems
and software. Since then I've been working on safety firmware and building the
common software platform our applications run on.

As a kid I was always math and science centric, and taught myself to program
in junior year from C++ and C# tutorials. Still, my primary interest was
Biology until I entered college and found my interests in bio-engineering put
the "fi" in sci-fi.

My coursework in college varied from VLSI design, where I architected and laid
out silicon for a content-addressable-memory, to distributed systems, where
I implemented a self-recovering consensus-based database. I spent a lot of time
in a high-performance computing research lab, and was exposed to a lot of
co-processor based acceleration techniques.

Outside of work I enjoy working on personal projects, some tech-related, some
other crafts such as bookbinding and model painting (I love 40k lore).
I also read a lot, some of my favorite recent books have been the Red Rising
series by Pierce Brown, a tale of genetically-induced class warfare across the
solar system, and the Cosmere series by Brandon Sanderson, a galaxy-spanning
tale of magic and morality.

Other than that I have a good collection of board games, and have an ongoing
ongoing DnD campaign in a setting I've been developing for a year now. It'll
likely spin into me writing a book from my player's adventures, I'm excited
for that day to come!

# Archive
<a href="/archive/past_about_mes">Past 'About Me's</a>

<a href="/archive/past_about_motds">Past 'Message of the Day's</a>

# Index
<ul>
  {% for post in site.posts %}
    <li>
      <a href="{{ post.url }}">{{post.date | date: "%D"}} - {{ post.title }}</a>
    </li>
  {% endfor %}
</ul>
