---
layout: default
---

# Past 'Message of the Day's

When I retire a MotD I'll move it here, with a brief reminder as to why I empathized with it.
