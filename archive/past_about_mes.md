---
layout: default
---

# Past "About Me"s

Everyone grows, inevitably an "About Me" becomes outdated. This is an archive
of them, to give context to my life.

## College though Graduation (May 2019)
I'm a Computer Engineering Masters Student at Northeastern University, with
a focus on systems and software.

I love technology, and could probably chat someone's ear off explaining how
computers work from the bottom up! From how IC's are constructed in-silicon to
high-performance computing, I've had experience with it all.

If you're thinking "Wow, this blog is shameless self promotion for a student"...
you're not entirely wrong. I've been writing technical blogs since my freshman
summer, when I got into using YACC/BISON to play around with parsing code, but
now that I'm approaching the time to start seeking a fulltime job I need to
start seriously working on how I present myself. This is a part of that effort.
